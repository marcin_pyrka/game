<?php

	namespace Engine\Game
	{

		use Engine\Game as Game;
		use Engine\Game\Resources as Resources;
		use Engine\Controller\Users as Users;
		use Framework\Registry as Registry;

		class City extends Game {

			/**
			 * @readonly
			 */
			public $_id;

			/**
			 * @readonly
			 */
			public $_name;

			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				$database = Registry::get ("database");
				$session = Registry::get ("session");
				$user = Registry::get ("user");
				$userID = $user->getActivID ();

				// var_dump( $user );

				$queryBuilder = $database->createQueryBuilder ();
				$queryBuilder->select ('*')->from ('cities')->where ('users_idusers = :users_idusers');
				$sql = $queryBuilder->getSql ();
				$stmt = $database->prepare ($sql);
				$stmt->bindValue ("users_idusers", $userID);
				$stmt->execute ();
				$city = $stmt->fetchAll ();

				$this->_id = $city [ 0 ] [ 'idcities' ];
				$this->_name = $city [ 0 ] [ 'cities_name' ];

				return $this;
			}

			public function _check_amount ($_supply)
			{
				return $_supply;
			}

			public function _tree ($options = array ())
			{
				return $this;
			}
		}
	}
