<?php

	namespace Engine\Game\City
	{

		use Engine\Game as Game;
		use Engine\Game\City as City;
		use Engine\Game\Resources as Resources;
		use Framework\Registry as Registry;

		class Buildings extends City {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}

			/**
			 *
			 * @return \Application\Game\City\Buildings
			 */
			public function dependence ($options = array ())
			{
				return $this;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Application\Game\City\Buildings
			 */
			public function buy ($options = array ())
			{
				return $this;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Application\Game\City\Buildings
			 */
			public function update ($options = array ())
			{
				return $this;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Application\Game\City::_tree()
			 */
			public function _tree ($options = array ())
			{
				parent::_tree ($options);

				return $options;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Application\Game\City\Buildings
			 */
			public function capacity ($options = array ())
			{
				return $this;
			}
		}
	}