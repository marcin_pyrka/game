<?php

	namespace Engine\Game\City\Buildings
	{

		use Engine\Game as Game;
		use Engine\Game\City as City;
		use Engine\Game\City\Buildings as Buildings;
		use Framework\Registry as Registry;

		class Residential extends Buildings {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Engine\Game\City\Residential
			 */
			public function dependence ($options = array ())
			{
				return $this;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Engine\Game\City\Buildings::buy()
			 */
			public function buy ($options = array ())
			{
				parent::buy ($options);

				return $this;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Engine\Game\City\Buildings::update()
			 */
			public function update ($options = array ())
			{
				parent::update ($options);

				return $this;
			}
		}
	}
