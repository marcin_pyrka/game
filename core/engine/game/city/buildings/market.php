<?php

	namespace Engine\Game\City\Buildings
	{

		use Engine\Game as Game;
		use Engine\Game\City as City;
		use Engine\Game\City\Buildings as Buildings;
		use Framework\Registry as Registry;

		class Market extends Buildings {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Engine\Game\City\Buildings::dependence()
			 */
			public function dependence ($options = array ())
			{
				parent::dependence ($options);

				return $this;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Engine\Game\City\Buildings::buy()
			 */
			public function buy ($options = array ())
			{
				parent::buy ($options);

				return $this;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Engine\Game\City\Buildings::update()
			 */
			public function update ($options = array ())
			{
				parent::update ($options);

				return $this;
			}
		}
	}
