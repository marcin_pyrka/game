<?php

	namespace Engine\Game\City\Buildings\Fortifications
	{

		use Engine\Game as Game;
		use Engine\Game\City as City;
		use Engine\Game\City\Buildings as Buildings;
		use Engine\Game\City\Buildings\Fortifications as Fortifications;
		use Framework\Registry as Registry;

		class Wall extends Fortifications {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Engine\Game\City\Buildings\Fortifications\Wall
			 */
			public function dependence ($options = array ())
			{
				parent::dependence ($options);

				return $this;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Engine\Game\City\Buildings\Fortifications\Wall
			 */
			public function buy ($options = array ())
			{
				parent::buy ($options);

				return $this;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Engine\Game\City\Buildings\Fortifications\Wall
			 */
			public function update ($options = array ())
			{
				parent::update ($options);

				return $this;
			}
		}
	}
