<?php

	namespace Engine\Game\City\Buildings\Residential
	{

		use Engine\Game as Game;
		use Engine\Game\City as City;
		use Engine\Game\City\Buildings as Buildings;
		use Engine\Game\City\Buildings\Residential as Residential;
		use Framework\Registry as Registry;

		class MudHut extends Residential {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Engine\Game\City\Buildings\Residential::dependence()
			 */
			public function dependence ($options = array ())
			{
				parent::dependence ($options);

				return $this;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Engine\Game\City\Buildings\Residential::buy()
			 */
			public function buy ($options = array ())
			{
				parent::buy ($options);

				return $this;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Engine\Game\City\Buildings\Residential::update()
			 */
			public function update ($options = array ())
			{
				parent::update ($options);

				return $this;
			}
		}
	}
