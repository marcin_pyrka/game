<?php

	namespace Engine\Game
	{

		use Engine\Game as Game;
		use Engine\Game\Resources as Resources;
		use Engine\Game\City as City;
		use Framework\Registry as Registry;

		class Inventions extends Game {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Engine\Game\Inventions
			 */
			public function dependence ($options = array ())
			{
				return $this;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Engine\Game\Inventions
			 */
			public function discover ($options = array ())
			{
				return $this;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Engine\Game\Inventions
			 */
			public function update ($options = array ())
			{
				return $this;
			}
		}
	}
