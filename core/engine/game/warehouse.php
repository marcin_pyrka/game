<?php

	namespace Engine\Game
	{

		use Engine\Game as Game;
		use Engine\Game\Resources as Resources;
		use Framework\Registry as Registry;

		class Warehouse extends Game {

			/**
			 * @readonly
			 *
			 * @var unknown
			 */
			public $_id;

			/**
			 * @readonly
			 *
			 * @var unknown
			 */
			public $_name;

			/**
			 *
			 * @param unknown $options
			 *
			 *
			 * @todo 1. jak się nie mylę, powtarzamy w tym elemencie bardzo wiele kodu - nie może tak to pozostać...
			 *
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				$database = Registry::get ("database");
				$session = Registry::get ("session");
				$user = Registry::get ("user");
				$userID = $user->getActivID ();

				return $this;
			}
		}
	}
