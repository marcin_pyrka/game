<?php

	namespace Engine\Game
	{

		use Engine\Game as Game;
		use Framework\Registry as Registry;

		class Resources extends Game {

			/**
			 * @readonly
			 *
			 * @var unknown
			 */
			public $_resources_list = array (
			  "brown",
			  "steel",
			  "boards",
			  "logs",
			  "milk",
			  "seeds",
			  "clay",
			  "coal",
			  "cooper",
			  "gold",
			  "iron",
			  "silver",
			  "stone",
			  "tin",
			  "wood",
			  "hatchet",
			  "plow"
			);

			/**
			 * Zapełniane jest zawartościa zgodną z listą powyżej...
			 *
			 * @var unknown
			 */
			public $_resources = array ();

			/**
			 * Nie powinno być czegoś takiego jak poniżej:
			 *
			 * @todo zamienić to na for albo foreach
			 */
			public $_brown;
			public $_brown_incom;
			public $_steel;
			public $_steel_incom;
			public $_boards;
			public $_boards_incom;
			public $_logs;
			public $_logs_incom;
			public $_milk;
			public $_milk_incom;
			public $_seeds;
			public $_seeds_incom;
			public $_clay;
			public $_clay_incom;
			public $_coal;
			public $_coal_incom;
			public $_cooper;
			public $_cooper_incom;
			public $_gold;
			public $_gold_incom;
			public $_iron;
			public $_iron_incom;
			public $_silver;
			public $_silver_incom;
			public $_stone;
			public $_stone_incom;
			public $_tin;
			public $_tin_incom;
			public $_wood;
			public $_wood_incom;
			public $_hatchet;
			public $_hatchet_incom;
			public $_plow;
			public $_plow_incom;

			/**
			 *
			 * @param unknown $options
			 * @return \Engine\Game\Resources
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				$database = Registry::get ("database");
				$session = Registry::get ("session");
				$user = Registry::get ("user");
				$userID = $user->getActivID ();

				$this->convert ($userID);
				$resources = $this->getResources ($userID);
				$resources_incom = $this->getResourcesIncom ($userID);

				// var_ ( $resources );
				// var_ ( $resources_incom );

				foreach ( $this->_resources_list as $resource )
				{
					// print $resource . " ";
					$resource_value = $resources [ 0 ] [ "resources_".$resource ];
					$resource_incom_value = $resources_incom [ 0 ] [ "resources_incom_".$resource ];
					// print $resource_value . " ";
					// print $resource_incom_value;
					// print "\n\r";

					$this->_resources [ $resource ] = array (
					  "name" => $resource,
					  "value" => $resource_value,
					  "incom" => $resource_incom_value
					);
				}

				// var_ ( $this->_resources );

				$this->_brown = floor ($resources [ 0 ] [ 'resources_brown' ]); // 1
				$this->_steel = floor ($resources [ 0 ] [ 'resources_steel' ]); // 2
				$this->_boards = floor ($resources [ 0 ] [ 'resources_boards' ]); // 3
				$this->_logs = floor ($resources [ 0 ] [ 'resources_logs' ]); // 4
				$this->_milk = floor ($resources [ 0 ] [ 'resources_milk' ]); // 5
				$this->_seeds = floor ($resources [ 0 ] [ 'resources_seeds' ]); // 6
				$this->_clay = floor ($resources [ 0 ] [ 'resources_clay' ]); // 7
				$this->_coal = floor ($resources [ 0 ] [ 'resources_coal' ]); // 8
				$this->_cooper = floor ($resources [ 0 ] [ 'resources_cooper' ]); // 9
				$this->_gold = floor ($resources [ 0 ] [ 'resources_gold' ]); // 10
				$this->_iron = floor ($resources [ 0 ] [ 'resources_iron' ]); // 11
				$this->_silver = floor ($resources [ 0 ] [ 'resources_silver' ]); // 12
				$this->_stone = floor ($resources [ 0 ] [ 'resources_stone' ]); // 13
				$this->_tin = floor ($resources [ 0 ] [ 'resources_tin' ]); // 14
				$this->_wood = floor ($resources [ 0 ] [ 'resources_wood' ]); // 15
				$this->_hatchet = floor ($resources [ 0 ] [ 'resources_hatchet' ]); // 16
				$this->_plow = floor ($resources [ 0 ] [ 'resources_plow' ]); // 17

				$this->_brown_incom = $resources_incom [ 0 ] [ 'resources_brown_incom' ]; // 1
				$this->_steel_incom = $resources_incom [ 0 ] [ 'resources_steel_incom' ]; // 2
				$this->_boards_incom = $resources_incom [ 0 ] [ 'resources_boards_incom' ]; // 3
				$this->_logs_incom = $resources_incom [ 0 ] [ 'resources_logs_incom' ]; // 4
				$this->_milk_incom = $resources_incom [ 0 ] [ 'resources_milk_incom' ]; // 5
				$this->_seeds_incom = $resources_incom [ 0 ] [ 'resources_seeds_incom' ]; // 6
				$this->_clay_incom = $resources_incom [ 0 ] [ 'resources_clay_incom' ]; // 7
				$this->_coal_incom = $resources_incom [ 0 ] [ 'resources_coal_incom' ]; // 8
				$this->_cooper_incom = $resources_incom [ 0 ] [ 'resources_cooper_incom' ]; // 9
				$this->_gold_incom = $resources_incom [ 0 ] [ 'resources_gold_incom' ]; // 10
				$this->_iron_incom = $resources_incom [ 0 ] [ 'resources_iron_incom' ]; // 11
				$this->_silver_incom = $resources_incom [ 0 ] [ 'resources_silver_incom' ]; // 12
				$this->_stone_incom = $resources_incom [ 0 ] [ 'resources_stone_incom' ]; // 13
				$this->_tin_incom = $resources_incom [ 0 ] [ 'resources_tin_incom' ]; // 14
				$this->_wood_incom = $resources_incom [ 0 ] [ 'resources_wood_incom' ]; // 15
				$this->_hatchet_incom = $resources_incom [ 0 ] [ 'resources_hatchet_incom' ]; // 16
				$this->_plow_incom = $resources_incom [ 0 ] [ 'resources_plow_incom' ]; // 17

				// var_dump ( $this );
				return $this;
			}

			/**
			 */
			public function add ()
			{
			}

			/**
			 */
			public function subtract ()
			{
			}

			/**
			 */
			public function upload ()
			{
			}

			/**
			 *
			 * @param unknown $supply
			 * @return unknown
			 */
			public function checkAmount ($supply)
			{
				$database = Registry::get ("database");
				$session = Registry::get ("session");

				return $result;
			}

			/**
			 *
			 * @param unknown $resources_users_idusers
			 * @return unknown
			 */
			public function getResources ($resources_users_idusers)
			{

				/**
				 */
				$database = Registry::get ("database");
				$session = Registry::get ("session");
				$user = Registry::get ("user");
				$userID = $user->getActivID ();

				/**
				 */
				$queryBuilder = $database->createQueryBuilder ();
				$queryBuilder->select ('*')->from ('resources')->where ('users_idusers = :users_idusers');
				$sql = $queryBuilder->getSql ();
				$stmt = $database->prepare ($sql);
				$stmt->bindValue ("users_idusers", $userID);

				$stmt->execute ();

				$resources = $stmt->fetchAll ();

				return $resources;
			}

			/**
			 *
			 * @param unknown $resources_users_idusers
			 * @return unknown
			 */
			public function getResourcesIncom ($resources_users_idusers)
			{

				/**
				 */
				$database = Registry::get ("database");
				$session = Registry::get ("session");
				$user = Registry::get ("user");
				$userID = $user->getActivID ();

				/**
				 */
				$queryBuilder = $database->createQueryBuilder ();
				$queryBuilder->select ('*')->from ('resources_incom')->where (
				  'resources_users_idusers = :resources_users_idusers'
				);
				$sql = $queryBuilder->getSql ();
				$stmt = $database->prepare ($sql);
				$stmt->bindValue ("resources_users_idusers", $userID);

				$stmt->execute ();

				$resources_incom = $stmt->fetchAll ();

				return $resources_incom;
			}

			/**
			 *
			 * @param unknown $resources_users_idusers
			 * @return \Engine\Game\Resources
			 */
			public function convert ($resources_users_idusers)
			{
				$database = Registry::get ("database");
				$session = Registry::get ("session");
				$user = Registry::get ("user");
				$userID = $user->getActivID ();

				// var_dump ( $resources );
				// var_dump ( $resources_incom );

				$time_act = time ();

				$resources = $this->getResources ($resources_users_idusers);
				$resources_incom = $this->getResourcesIncom ($resources_users_idusers);

				// var_dump ( $time_act );

				$difference = $time_act - $resources_incom [ 0 ] [ 'resources_incom_convert_last_time' ];

				$new_gold = $resources [ 0 ] [ 'resources_gold' ] + ( $resources_incom [ 0 ] [ 'resources_incom_gold' ] / 60 / 60 * $difference );
				$new_wood = $resources [ 0 ] [ 'resources_wood' ] + ( $resources_incom [ 0 ] [ 'resources_incom_wood' ] / 60 / 60 * $difference );
				$new_stone = $resources [ 0 ] [ 'resources_stone' ] + ( $resources_incom [ 0 ] [ 'resources_incom_stone' ] / 60 / 60 * $difference );

				$queryBuilder = $database->createQueryBuilder ();

				$queryBuilder->update ('resources')->set ('resources_gold', $new_gold)->set (
				  'resources_wood',
				  $new_wood
				)->set ('resources_stone', $new_stone)->where ('users_idusers = :users_idusers');
				$sql = $queryBuilder->getSql ();
				$stmt = $database->prepare ($sql);
				$stmt->bindValue ("users_idusers", $resources_users_idusers);
				$stmt->execute ();

				$queryBuilder = $database->createQueryBuilder ();
				$queryBuilder->update ('resources_incom')->set ('resources_incom_convert_last_time', $time_act)->where (
				  'resources_users_idusers = :resources_users_idusers'
				);
				$sql = $queryBuilder->getSql ();
				$stmt = $database->prepare ($sql);
				$stmt->bindValue ("resources_users_idusers", $resources_users_idusers);
				$stmt->execute ();

				return $this;
			}
		}
	}
