<?php

	namespace Engine\Game
	{

		use Engine\Game as Game;
		use Framework\Registry as Registry;

		class Treasury extends Game {

			/**
			 * @readonly
			 *
			 * @var unknown
			 */
			public $_id;

			/**
			 * @readonly
			 *
			 * @var unknown
			 */
			public $_name;

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				$database = Registry::get ("database");
				$session = Registry::get ("session");

				$queryBuilder = $database->createQueryBuilder ();
				$queryBuilder->select ('idusers', 'users_email')->from ('users')->where ('users_name = :user_name');
				$sql = $queryBuilder->getSql ();
				$stmt = $database->prepare ($sql);
				$stmt->bindValue ("user_name", $session->getup ('user'));

				$stmt->execute ();
				$user = $stmt->fetchAll ();

				$queryBuilder = $database->createQueryBuilder ();
				$queryBuilder->select ('*')->from ('cities')->where ('users_idusers = :users_idusers');
				$sql = $queryBuilder->getSql ();
				$stmt = $database->prepare ($sql);
				$stmt->bindValue ("users_idusers", $user [ 0 ] [ 'idusers' ]);
				$stmt->execute ();
				$city = $stmt->fetchAll ();

				$this->_id = $city [ 0 ] [ 'idcities' ];
				$this->_name = $city [ 0 ] [ 'cities_name' ];

				return $this;
			}
		}
	}
