<?php

	namespace Engine\Game\Resources\BuildingMaterials
	{

		use Engine\Game as Game;
		use Engine\Game\Resources as Resources;
		use Engine\Game\Resources\BuildingMaterials as BuildingMaterials;
		use Framework\Registry as Registry;

		class Bricks extends BuildingMaterials {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}
		}
	}
