<?php

	namespace Application\Game\Resources
	{

		use Application\Game as Game;
		use Application\Game\Resources as Resources;
		use Framework\Registry as Registry;

		class BuildingMaterials extends Resources {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}
		}
	}
