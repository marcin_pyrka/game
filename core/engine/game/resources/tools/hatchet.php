<?php

	namespace Engine\Game\Resources\Tools
	{

		use Engine\Game as Game;
		use Engine\Game\Resources as Resources;
		use Engine\Game\Resources\Tools as Tools;
		use Framework\Registry as Registry;

		class Hatchet extends Tools {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Application\Game\Resources\Tools::dependence()
			 */
			public function dependence ($options = array ())
			{
				parent::dependence ($options);

				return $this;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Application\Game\Resources\Tools::factory()
			 */
			public function factory ($options = array ())
			{
				parent::factory ($options);

				return $this;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Application\Game\Resources\Tools::storage()
			 */
			public function storage ($options = array ())
			{
				parent::storage ($options);

				return $this;
			}
		}
	}
