<?php

	namespace Engine\Game\Resources\Foods
	{

		use Engine\Game as Game;
		use Engine\Game\Resources as Resources;
		use Engine\Game\Resources\Foods as Foods;
		use Framework\Registry as Registry;

		class Seeds extends Foods {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}
		}
	}
