<?php

	namespace Engine\Game\Resources\Minerals
	{

		use Engine\Game as Game;
		use Engine\Game\Resources as Resources;
		use Engine\Game\Resources\Minerals as Minerals;
		use Framework\Registry as Registry;

		class Gold extends Minerals {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}
		}
	}
