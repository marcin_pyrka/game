<?php

	namespace Engine\Game\Resources
	{

		use Engine\Game as Game;
		use Engine\Game\Resources as Resources;
		use Framework\Registry as Registry;

		class Tools extends Resources {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Application\Game\Resources\Tools
			 */
			public function dependence ($options = array ())
			{
				return $this;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Application\Game\Resources\Tools
			 */
			public function factory ($options = array ())
			{
				return $this;
			}

			/**
			 *
			 * @param unknown $options
			 * @return \Application\Game\Resources\Tools
			 */
			public function storage ($options = array ())
			{
				return $this;
			}
		}
	}