<?php

	namespace Engine\Game\Resources\Alloys
	{

		use Engine\Game as Game;
		use Engine\Game\Resources as Resources;
		use Engine\Game\Resources\Alloys as Alloys;
		use Framework\Registry as Registry;

		class Steel extends Alloys {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);

				return $this;
			}
		}
	}
