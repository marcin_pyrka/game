<?php

/**
 * Należy przepisać całość zapytań na DBAL->MySQL
 */
namespace Framework\Database {

	use Framework\Database as Database;
	use Framework\Database\Exception as Exception;
	use Framework\ArrayMethods as ArrayMethods;
	use Doctrine as Doctrine;
	use Doctrine\ORM\Tools\Setup;
	use Doctrine\ORM\EntityManager;

	/**
	 *
	 * @author Marcin 'jetAlone' Pyrka, pyrka.marcin@gmail.com
	 */
	class DBAL extends Database {
		
		/**
		 * @readwrite
		 */
		public $_config;
		
		/**
		 * @readwrite
		 */
		protected $_connectionParams;
		
		/**
		 * @readwrite
		 */
		public $_conn;
		
		/**
		 *
		 * @param unknown $_options        	
		 */
		public function __construct() {
			// $isDevMode = true;
			
			// $this->_config = new \Doctrine\DBAL\Configuration ();
			
			// $this->_config = Setup::createAnnotationMetadataConfiguration ( array (
			// __DIR__ . "/src"
			// ), $isDevMode );
			// $this->_connectionParams = array (
			// 'driver' => 'pdo_mysql',
			// 'host' => 'localhost',
			// 'dbname' => 'game',
			// 'user' => 'root',
			// 'password' => '',
			// 'charset' => 'utf8',
			// 'driverOptions' => array (
			// 1002 => 'SET NAMES utf8'
			// )
			// );
			if (getenv ( 'OPENSHIFT_MYSQL_DB_HOST' )) {
				
				$this->_connectionParams = array (
						'driver' => 'pdo_mysql',
						'host' => getenv ( 'OPENSHIFT_MYSQL_DB_HOST' ),
						'dbname' => "game",
						'user' => getenv ( 'OPENSHIFT_MYSQL_DB_USERNAME' ),
						'password' => getenv ( 'OPENSHIFT_MYSQL_DB_PASSWORD' ),
						'charset' => 'utf8',
						'driverOptions' => array (
								1002 => 'SET NAMES utf8' 
						) 
				);
			} else {
				$this->_connectionParams = array (
						'driver' => 'pdo_mysql',
						'host' => 'localhost',
						'dbname' => 'game',
						'user' => 'root',
						'password' => '',
						'charset' => 'utf8',
						'driverOptions' => array (
								1002 => 'SET NAMES utf8' 
						) 
				);
			}
			
			// $driver = new \Doctrine\ORM\Mapping\Driver\YamlDriver ( array (
			// './database/ERP.dcm.yml'
			// ) );
			
			// $this->_conn = \Doctrine\DBAL\DriverManager::getConnection ( $this->_connectionParams, $this->_config );
			
			// $paths = array (
			// APP_DIR . "./../generated/Entity.User.dcm.yml"
			// );
			// $this->_config = Setup::createYAMLMetadataConfiguration ( $paths, $isDevMode );
			// $this->_conn = EntityManager::create ( $this->_connectionParams, $this->_config );
			
			$this->_config = new \Doctrine\DBAL\Configuration ();
			$this->_conn = \Doctrine\DBAL\DriverManager::getConnection ( $this->_connectionParams, $this->_config );
			// print '<pre>';
			// var_dump ( $this->_config );
			// print '</pre>';
			
			// print '<pre>';
			// var_dump ( $this->_conn );
			// print '</pre>';
			
			return $this->_conn;
		}
	}
}
