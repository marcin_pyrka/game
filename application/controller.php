<?php

	/**
	 *
	 */
	namespace Application
	{

		use Framework\Events as Events;
		use Framework\Registry as Registry;
		use Framework\Session as Session;
		use Framework\Request as Request;
		use Framework\RequestMethods as RequestMethods;
		use Application\Controller\Users as Users;
		use Engine\Game as Game;
		use Engine\Game\City as City;
		use Engine\Game\City\Buildings as Buildings;
		use Engine\Game\Resources as Resources;

		/**
		 *
		 * @author Marcin
		 *
		 */
		class Controller extends \Framework\Controller {

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				parent::__construct ($options);
			}

			/**
			 */
			public function init ()
			{

				/**
				 * $database = Registry::get ( "database" );
				 * $session = Registry::get ( "session" );
				 * $user = Registry::get ( "user" );
				 */
			}

			/**
			 *
			 * @todo :
			 *       1. zmienić jakoś te game, aby bardziej to wyglądało na wykorzystanie klas
			 *       oraz przestrzeni nazw, bo się syf zrobił
			 *       2. naprawić authenticate, bo siara...
			 *
			 */
			public function game ()
			{
				$database = Registry::get ("database");
				$session = Registry::get ("session");

				$user = new Users ($options);
				\Framework\Registry::set ("user", $user);

				$game = new Game ($options);
				\Framework\Registry::set ("game", $game);

				$city = new City ($options);
				\Framework\Registry::set ("city", $city);

				$resources = new Resources ($options);
				\Framework\Registry::set ("resources", $resources);

				/**
				 */
				// TEST!
				// $test->dependence ( $options );
				// $test = new Buildings\Residential\BrickHouse ( $options );
				// $test = new Buildings\Fortifications\Wall ( $options );
				// $test = new Resources\Tools\Hatchet ( $options );

				// print '<pre>';
				// var_dump ( $test_Buildings_Buildings );
				// var_dump ( $test_Buildings_Residential_BrickHouse );
				// var_dump ( $test_Buildings_Fortifications_Wall );

				// $Buildings = new Buildings ( $options );
				// var_dump ( $Buildings->_tree ( array (
				// 'test' => 'tresc testu'
				// ) ) );

				// var_dump ( $resources );

				// $user = Users::getActivID ( $options );

				// var_dump ( $user->getActivData ( $options = null ) );

				// print '</pre>';

				// KONIEC TESTU!
			}

			/**
			 */
			public function authenticate ()
			{
				$database = Registry::get ("database");
				$session = Registry::get ("session");

				if ( $session->getup ('user') )
				{

					if ( RequestMethods::get (url) === 'users/index' )
					{
						header ("Location: home/index");
					}

					$queryBuilder = $database->createQueryBuilder ();
					$queryBuilder->select ('idusers', 'users_email')->from ('users')->where ('users_name = :user_name');
					$sql = $queryBuilder->getSql ();
					$stmt = $database->prepare ($sql);
					$stmt->bindValue ("user_name", $session->getup ('user'));

					$stmt->execute ();
					$user = $stmt->fetchAll ();

					\Framework\Registry::set ("user_id", $user [ 0 ] [ 'idusers' ]);
				} elseif ( RequestMethods::get (url) === 'users/index' )
				{
				} else
				{
					header ("Location: users/index");
				}
			}

			/**
			 */
			public function notify ()
			{
			}
		}
	}
