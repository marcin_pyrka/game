<?php

	namespace Application\Controller
	{

		use Application\Controller as Controller;
		use Framework\Registry as Registry;
		use Framework\RequestMethods as RequestMethods;
		use Framework\Session\Driver\Server;
		use Framework\Request as Request;

		/**
		 *
		 * @author Marcin
		 *
		 */
		class Users extends Controller {

			/**
			 * @readwrite
			 */
			protected $_parameters;

			/**
			 * @readwrite
			 */
			protected $_table = array ();

			/**
			 * @readonly
			 */
			public $ID = null;

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options)
			{
				/**
				 */
				$this->_parameters = $options [ 'parameters' ];
			}

			public function givmetable ()
			{
				return ( $this->_table );
			}

			/**
			 * @once @protected (non-PHPdoc)
			 *
			 * @see \Application\Controller::init()
			 */
			public function init ()
			{
				parent::init ();
			}

			/**
			 * @protected (non-PHPdoc)
			 *
			 * @see \Application\Controller::authenticate()
			 */
			public function authenticate ()
			{
				parent::authenticate ();
			}

			/**
			 * @once @protected
			 */
			/**
			 * (non-PHPdoc)
			 *
			 * @see \Application\Controller::notify()
			 */
			public function notify ()
			{
				parent::notify ();
			}

			/**
			 * @before init, authenticate, @after notify
			 */
			/**
			 */
			public function index ()
			{
			}

			/**
			 * @before init @after notify
			 */
			/**
			 */
			public function logout ()
			{
				$session = Registry::get ("session");
				$session->setup ("user", false);
				header ("Location: /users/index");
				exit ();
			}

			/**
			 * @before init @after notify
			 *
			 * @todo 1. naprawić jakoś to logowanie
			 *       2. i czy musi być wszystko tak na brutala?
			 *
			 */
			public function login ()
			{
				if ( RequestMethods::post ("login") )
				{

					/**
					 * @TODO - sprawdzić działanie requestmethods - koniecznie przez requestami zbudować form logowania :D
					 */
					$name = RequestMethods::post ("name");
					$password = RequestMethods::post ("password");

					/**
					 * @TODO - zmienić odwołanie dla view na zgodne z aktualnym - poprawić komunukacje z widokiem
					 */
					$error = false;
					if ( empty ( $name ) )
					{
						$error = true;
					}
					if ( empty ( $password ) )
					{
						$error = true;
					}
					if ( !$error )
					{

						$database = Registry::get ("database");
						$user_in_database = $database->fetchAll (
						  "SELECT * FROM `users` WHERE `users_name`='".$name."' and `users_password`='".$password."'"
						);

						if ( $user_in_database [ 0 ] )
						{
							$user = $user_in_database [ 0 ] [ 'users_name' ];
						} else
						{
							$error = true;
						}

						if ( !empty ( $user ) )
						{
							$session = Registry::get ("session");
							$session->setup ("user", $user_in_database [ 0 ] [ 'users_name' ]);
							header ("Location: /");
							exit ();
						} else
						{

							/**
							 *
							 * @todo 1. błędy logowania!!!
							 */

							header ("Location: /users/index");
							exit ();
						}
						exit ();
					}
				}
			}

			/**
			 * @before init, @after notify
			 *
			 * @todo 1. wykonać dodawanie nowych userów - pamiętać o rozbijaniu funkcji na mniejsze, do wielokrotnego wykorzystania,
			 *       znajdujące się w odpowiednich miejscach...
			 */
			public function signup ()
			{
			}

			/**
			 *
			 * @param unknown $option
			 * @return unknown
			 */
			public function getActivData ($option = array ())
			{
				$database = Registry::get ("database");
				$session = Registry::get ("session");

				// $this->convert ( $user [0] ['idusers'] );
				$queryBuilder = $database->createQueryBuilder ();
				$queryBuilder->select ('idusers', 'users_email')->from ('users')->where ('users_name = :user_name');
				$sql = $queryBuilder->getSql ();
				$stmt = $database->prepare ($sql);
				$stmt->bindValue ("user_name", $session->getup ('user'));

				$stmt->execute ();
				$user = $stmt->fetchAll ();

				// $this->convert ( $user [0] ['idusers'] );
				return $user;
			}

			/**
			 *
			 * @param unknown $option
			 * @return unknown
			 */
			public function getActivID ($option = array ())
			{
				$data = $this->getActivData ($option);

				return $data [ 0 ] [ "idusers" ];
			}

			public function getID ()
			{
				return $this->ID;
			}
		}
	}