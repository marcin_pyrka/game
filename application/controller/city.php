<?php

	namespace Application\Controller
	{

		use Application\Controller as Controller;
		use Framework\Registry as Registry;
		use Framework\RequestMethods as RequestMethods;
		use Framework\Session as Session;
		use Framework\View as View;
		use Framework\Request as Request;
		use Framework\Events as Events;

		/**
		 *
		 * @author Marcin
		 *
		 */
		class City extends Controller {

			/**
			 * @readwrite
			 */
			protected $_parameters;

			/**
			 * @readwrite
			 */
			protected $_table = array ();

			/**
			 * @readwrite
			 */
			protected $_options;

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				$this->_parameters = $options [ 'parameters' ];
			}

			public function givmetable ()
			{
				return ( $this->_table );
			}

			/**
			 * @once @protected
			 */
			public function init ()
			{
				parent::init ();
				Events::fire (
				  "framework.controller.construct.before",
				  array (
					$this->name
				  )
				);
			}

			/**
			 * @protected
			 */
			public function authenticate ()
			{
				parent::authenticate ();
			}

			/**
			 * @protected
			 */
			public function game ()
			{
				parent::game ();

				$game = Registry::get ("game");
				$resources = Registry::get ("resources");
				$this->_table [ 'resources' ] [ 'resources_gold' ] = $resources->_gold;
				$this->_table [ 'resources' ] [ 'resources_wood' ] = $resources->_wood;
				$this->_table [ 'resources' ] [ 'resources_stone' ] = $resources->_stone;

				$this->_table [ 'resources' ] [ 'resources_incom_gold' ] = $resources->_gold_incom;
				$this->_table [ 'resources' ] [ 'resources_incom_wood' ] = $resources->_wood_incom;
				$this->_table [ 'resources' ] [ 'resources_incom_stone' ] = $resources->_stone_incom;
			}

			/**
			 * @protected
			 */
			public function notify ()
			{
				parent::notify ();
			}

			/**
			 * @before init, authenticate, game, @after notify
			 */
			public function index ()
			{
				$database = Registry::get ("database");
				$game = Registry::get ("game");
				$city = Registry::get ("city");

				$this->_table [ 'city' ] [ '_id' ] = $city->_id;
				$this->_table [ 'city' ] [ '_name' ] = $city->_name;
			}
		}
	}