<?php

	namespace Application\Controller
	{

		use Application\Controller as Controller;
		use Framework\Registry as Registry;
		use Framework\RequestMethods as RequestMethods;
		use Framework\Session as Session;
		use Framework\View as View;
		use Framework\Request as Request;
		use Framework\Events as Events;
		use Framework\ArrayMethods;

		/**
		 *
		 * @author Marcin
		 *
		 */
		class Home extends Controller {

			/**
			 * @readwrite
			 */
			protected $_parameters;

			/**
			 * @readwrite
			 */
			protected $_table = array ();

			/**
			 * @readwrite
			 */
			protected $_options;

			/**
			 *
			 * @param unknown $options
			 */
			public function __construct ($options = array ())
			{
				$this->_parameters = $options [ 'parameters' ];
			}

			/**
			 *
			 * @return multitype:
			 */
			public function givmetable ()
			{
				return ( $this->_table );
			}

			/**
			 * @once @protected
			 * (non-PHPdoc)
			 *
			 * @see \Application\Controller::init()
			 */
			public function init ()
			{
				parent::init ();
				Events::fire (
				  "framework.controller.construct.before",
				  array (
					$this->name
				  )
				);
			}

			/**
			 * @protected
			 * (non-PHPdoc)
			 *
			 * @see \Application\Controller::authenticate()
			 */
			public function authenticate ()
			{
				parent::authenticate ();
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Application\Controller::game()
			 *
			 * @todo 1. nie można na sztywno przekazywać takich wartości - z biegiem czasu będzie ich
			 *       zbyt wiele, żeby można to było pilnować - do poprawki!
			 *       2. czemu funkcja w controller\home robi takie rzeczy? to nie ma sensu...?
			 */
			public function game ()
			{
				parent::game ();

				$game = Registry::get ("game");
				$resources = Registry::get ("resources");

				// $resources_to_screen = ArrayMethods::toObject ( $resources->_resources );
				$resources_to_screen = $resources->_resources;

				// var_ ( $resources_to_screen );
				$this->_table [ 'resources' ] [ 'resources_gold' ] = $resources->_gold;
				$this->_table [ 'resources' ] [ 'resources_wood' ] = $resources->_wood;
				$this->_table [ 'resources' ] [ 'resources_stone' ] = $resources->_stone;

				$this->_table [ 'resources' ] [ 'resources_incom_gold' ] = $resources->_gold_incom;
				$this->_table [ 'resources' ] [ 'resources_incom_wood' ] = $resources->_wood_incom;
				$this->_table [ 'resources' ] [ 'resources_incom_stone' ] = $resources->_stone_incom;
			}

			/**
			 * (non-PHPdoc)
			 *
			 * @see \Application\Controller::notify()
			 *
			 * @todo przydały by się logi...
			 */
			public function notify ()
			{
				parent::notify ();
			}

			/**
			 * @before init, authenticate, game, @after notify
			 */
			public function index ()
			{
				$database = Registry::get ("database");
				$game = Registry::get ("game");
				$city = Registry::get ("city");

				$this->_table [ 'city' ] [ '_id' ] = $city->_id;
				$this->_table [ 'city' ] [ '_name' ] = $city->_name;
			}

			/**
			 * @before init, authenticate, game, @after notify
			 */
			public function city ()
			{
				$game = Registry::get ("game");
				$city = Registry::get ("city");

				$this->_table [ 'city' ] [ '_id' ] = $city->_id;
				$this->_table [ 'city' ] [ '_name' ] = $city->_name;
			}

			/**
			 * @before init, authenticate, game, @after notify
			 */
			public function warehouse ()
			{
				$game = Registry::get ("game");
				$city = Registry::get ("city");

				$this->_table [ 'city' ] [ '_id' ] = $city->_id;
				$this->_table [ 'city' ] [ '_name' ] = $city->_name;
			}

			/**
			 * @before init, authenticate, game, @after notify
			 */
			public function treasury ()
			{
				$game = Registry::get ("game");
				$city = Registry::get ("city");

				$this->_table [ 'city' ] [ '_id' ] = $city->_id;
				$this->_table [ 'city' ] [ '_name' ] = $city->_name;
			}
		}
	}