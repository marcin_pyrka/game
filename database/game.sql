-- MySQL Script generated by MySQL Workbench
-- 03/26/15 11:18:03
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema game
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `game` ;

-- -----------------------------------------------------
-- Schema game
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `game` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `game` ;

-- -----------------------------------------------------
-- Table `game`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `game`.`users` ;

CREATE TABLE IF NOT EXISTS `game`.`users` (
  `idusers` INT NOT NULL,
  `users_email` VARCHAR(45) NOT NULL,
  `users_name` VARCHAR(45) NOT NULL,
  `users_password` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`idusers`, `users_name`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `game`.`cities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `game`.`cities` ;

CREATE TABLE IF NOT EXISTS `game`.`cities` (
  `idcities` INT NOT NULL,
  `cities_name` VARCHAR(256) NOT NULL,
  `users_idusers` INT NOT NULL,
  PRIMARY KEY (`idcities`, `users_idusers`),
  CONSTRAINT `fk_cities_users`
    FOREIGN KEY (`users_idusers`)
    REFERENCES `game`.`users` (`idusers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_cities_users_idx` ON `game`.`cities` (`users_idusers` ASC);


-- -----------------------------------------------------
-- Table `game`.`resources`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `game`.`resources` ;

CREATE TABLE IF NOT EXISTS `game`.`resources` (
  `idresources` INT NOT NULL,
  `users_idusers` INT NOT NULL,
  `cities_idcities` INT NOT NULL,
  `resources_brown` DOUBLE NULL,
  `resources_steel` DOUBLE NULL,
  `resources_boards` DOUBLE NULL,
  `resources_logs` DOUBLE NULL,
  `resources_milk` DOUBLE NULL,
  `resources_seeds` DOUBLE NULL,
  `resources_clay` DOUBLE NULL,
  `resources_coal` DOUBLE NULL,
  `resources_cooper` DOUBLE NULL,
  `resources_gold` DOUBLE NULL,
  `resources_iron` DOUBLE NULL,
  `resources_silver` DOUBLE NULL,
  `resources_stone` DOUBLE NULL,
  `resources_tin` DOUBLE NULL,
  `resources_wood` DOUBLE NULL,
  `resources_hatchet` DOUBLE NULL,
  `resources_plow` DOUBLE NULL,
  PRIMARY KEY (`idresources`, `users_idusers`, `cities_idcities`),
  CONSTRAINT `fk_resources_users1`
    FOREIGN KEY (`users_idusers`)
    REFERENCES `game`.`users` (`idusers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_resources_cities1`
    FOREIGN KEY (`cities_idcities`)
    REFERENCES `game`.`cities` (`idcities`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_resources_users1_idx` ON `game`.`resources` (`users_idusers` ASC);

CREATE INDEX `fk_resources_cities1_idx` ON `game`.`resources` (`cities_idcities` ASC);


-- -----------------------------------------------------
-- Table `game`.`resources_incom`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `game`.`resources_incom` ;

CREATE TABLE IF NOT EXISTS `game`.`resources_incom` (
  `idresources_incom` INT NOT NULL,
  `resources_idresources` INT NOT NULL,
  `resources_users_idusers` INT NOT NULL,
  `resources_cities_idcities` INT NOT NULL,
  `resources_incom_convert_last_time` INT NOT NULL,
  `resources_incom_brown` INT NULL DEFAULT 0,
  `resources_incom_steel` INT NULL DEFAULT 0,
  `resources_incom_boards` INT NULL DEFAULT 0,
  `resources_incom_logs` INT NULL DEFAULT 0,
  `resources_incom_milk` INT NULL DEFAULT 0,
  `resources_incom_seeds` INT NULL DEFAULT 0,
  `resources_incom_clay` INT NULL DEFAULT 0,
  `resources_incom_coal` INT NULL DEFAULT 0,
  `resources_incom_cooper` INT NULL DEFAULT 0,
  `resources_incom_gold` INT NULL DEFAULT 0,
  `resources_incom_iron` INT NULL DEFAULT 0,
  `resources_incom_silver` INT NULL DEFAULT 0,
  `resources_incom_stone` INT NULL DEFAULT 0,
  `resources_incom_tin` INT NULL DEFAULT 0,
  `resources_incom_wood` INT NULL DEFAULT 0,
  `resources_incom_hatchet` INT NULL DEFAULT 0,
  `resources_incom_plow` INT NULL DEFAULT 0,
  PRIMARY KEY (`idresources_incom`, `resources_idresources`, `resources_users_idusers`, `resources_cities_idcities`),
  CONSTRAINT `fk_resources_incom_resources1`
    FOREIGN KEY (`resources_idresources` , `resources_users_idusers` , `resources_cities_idcities`)
    REFERENCES `game`.`resources` (`idresources` , `users_idusers` , `cities_idcities`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_resources_incom_resources1_idx` ON `game`.`resources_incom` (`resources_idresources` ASC, `resources_users_idusers` ASC, `resources_cities_idcities` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `game`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `game`;
INSERT INTO `game`.`users` (`idusers`, `users_email`, `users_name`, `users_password`) VALUES (1, 'admin@admin.com', 'admin', 'admin');
INSERT INTO `game`.`users` (`idusers`, `users_email`, `users_name`, `users_password`) VALUES (2, 'pyrka.marcin@gmail.com', 'jetalone', 'password');

COMMIT;


-- -----------------------------------------------------
-- Data for table `game`.`cities`
-- -----------------------------------------------------
START TRANSACTION;
USE `game`;
INSERT INTO `game`.`cities` (`idcities`, `cities_name`, `users_idusers`) VALUES (1, 'admin', 1);
INSERT INTO `game`.`cities` (`idcities`, `cities_name`, `users_idusers`) VALUES (2, 'Moria', 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `game`.`resources`
-- -----------------------------------------------------
START TRANSACTION;
USE `game`;
INSERT INTO `game`.`resources` (`idresources`, `users_idusers`, `cities_idcities`, `resources_brown`, `resources_steel`, `resources_boards`, `resources_logs`, `resources_milk`, `resources_seeds`, `resources_clay`, `resources_coal`, `resources_cooper`, `resources_gold`, `resources_iron`, `resources_silver`, `resources_stone`, `resources_tin`, `resources_wood`, `resources_hatchet`, `resources_plow`) VALUES (1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL);
INSERT INTO `game`.`resources` (`idresources`, `users_idusers`, `cities_idcities`, `resources_brown`, `resources_steel`, `resources_boards`, `resources_logs`, `resources_milk`, `resources_seeds`, `resources_clay`, `resources_coal`, `resources_cooper`, `resources_gold`, `resources_iron`, `resources_silver`, `resources_stone`, `resources_tin`, `resources_wood`, `resources_hatchet`, `resources_plow`) VALUES (2, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `game`.`resources_incom`
-- -----------------------------------------------------
START TRANSACTION;
USE `game`;
INSERT INTO `game`.`resources_incom` (`idresources_incom`, `resources_idresources`, `resources_users_idusers`, `resources_cities_idcities`, `resources_incom_convert_last_time`, `resources_incom_brown`, `resources_incom_steel`, `resources_incom_boards`, `resources_incom_logs`, `resources_incom_milk`, `resources_incom_seeds`, `resources_incom_clay`, `resources_incom_coal`, `resources_incom_cooper`, `resources_incom_gold`, `resources_incom_iron`, `resources_incom_silver`, `resources_incom_stone`, `resources_incom_tin`, `resources_incom_wood`, `resources_incom_hatchet`, `resources_incom_plow`) VALUES (1, 1, 1, 1, 1418163941, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, 1, NULL, NULL);
INSERT INTO `game`.`resources_incom` (`idresources_incom`, `resources_idresources`, `resources_users_idusers`, `resources_cities_idcities`, `resources_incom_convert_last_time`, `resources_incom_brown`, `resources_incom_steel`, `resources_incom_boards`, `resources_incom_logs`, `resources_incom_milk`, `resources_incom_seeds`, `resources_incom_clay`, `resources_incom_coal`, `resources_incom_cooper`, `resources_incom_gold`, `resources_incom_iron`, `resources_incom_silver`, `resources_incom_stone`, `resources_incom_tin`, `resources_incom_wood`, `resources_incom_hatchet`, `resources_incom_plow`) VALUES (2, 2, 2, 2, 1418160000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 250, NULL, NULL, 80, NULL, 80, NULL, NULL);

COMMIT;

