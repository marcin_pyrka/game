<?php

	function var_ ($expression)
	{
		print '<pre>';
		var_dump ($expression);
		print '</pre>';
	}

	define ('APP_DIR', dirname (realpath (__FILE__)));
	define ('DISPLAY_ERRORS', 1);
	error_reporting (E_ERROR);
	ini_set ("display_errors", DISPLAY_ERRORS);
	require_once '../vendor/autoload.php';
	use Framework as Framework;

	$session = new Framework\Session ();
	$session = $session->initialize ();
	Framework\Registry::set ("session", $session);
	$request = new Framework\Request ();
	Framework\Registry::set ("request", $request);
	$database = new Framework\Database ();
	$database->initialize ();
	Framework\Registry::set ("database", $database->_dbal->_conn);
	$controller = new Application\Controller ();
